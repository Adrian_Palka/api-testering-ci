import requests


class GoRESTHandler:
    base_url = "https://gorest.co.in/public/v2"
    users_endpoint = "/users"

    headers = {
        "Authorization": "Bearer c1a372ed21bf3b4359bc007e7b472cccf2cb0fdac34ed9522363c0b02920d73f"
    }

    def create_user(self, user_data, expected_status_code=201):
        response = requests.post(self.base_url + self.users_endpoint, json=user_data, headers=self.headers)
        assert response.status_code == expected_status_code
        return response

    def get_user(self, user_id):
        response = requests.get(f"{self.base_url}{self.users_endpoint}/{user_id}", headers=self.headers)
        assert response.status_code == 200
        return response

    def patch_user(self, user_id, new_user_data):
        response = requests.patch(f"{self.base_url}{self.users_endpoint}/{user_id}", json=new_user_data,
                                  headers=self.headers)
        assert response.status_code == 200
        return response

    def delete_user(self, user_id):
        response = requests.delete(f"{self.base_url}{self.users_endpoint}/{user_id}", headers=self.headers)
        assert response.status_code == 204
        return response
