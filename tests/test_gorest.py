from faker import Faker
from utils.gorest_handler import GoRESTHandler

gorest_handler = GoRESTHandler()


def test_create_user():
    """Go Rest API: creating user + verifying user"""
    user_data = {
        "name": "Tenali Ramakrishna",
        "gender": "male",
        "email": Faker().email(),
        "status": "active"
    }
    body = gorest_handler.create_user(user_data).json()
    assert "id" in body
    user_id = body["id"]
    body = gorest_handler.get_user(user_id).json()
    assert body["email"] == user_data["email"]
    assert body["name"] == user_data["name"]
