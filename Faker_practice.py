from faker import Faker

generated_user_by_faker = Faker().name_male()
mail = f"{generated_user_by_faker}@example.com"
mail_2 = mail.replace(" ", "")

print(generated_user_by_faker, f"{generated_user_by_faker}@example.com")
print(generated_user_by_faker, f"{generated_user_by_faker}@example.com".replace(" ", "").lower())
print(generated_user_by_faker, f"{generated_user_by_faker}@example.com".lstrip())
print(generated_user_by_faker, mail.rstrip())
print(generated_user_by_faker, mail.strip())
print(generated_user_by_faker, mail.replace(" ", ""))
print(generated_user_by_faker, mail.replace(" ", "").lower())
print(generated_user_by_faker, mail_2.lower())

random_integer_mail = Faker().random_int(min=0, max=150000)
print(random_integer_mail)
print(f"johny_bravo{random_integer_mail}@example".replace(" ", ""))